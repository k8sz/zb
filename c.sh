#! /bin/bash
IP=`curl -i -s cip.cc |grep cip.cc |awk -F/ '{print $4}'`
client=`tr -dc 'A-Za-z' </dev/urandom | head -c 5`
cd /etc/openvpn/server/easy-rsa
./easyrsa --batch --days=3650 build-client-full "$client" nopass
echo "client
dev tun
proto udp
remote $IP 8443
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
auth SHA512
ignore-unknown-option block-outside-dns
verb 3"  > ~/"$client.ovpn"
echo "<ca>" >> ~/"$client.ovpn"
cat /etc/openvpn/server/easy-rsa/pki/ca.crt >> ~/"$client.ovpn"
echo "</ca>" >> ~/"$client.ovpn"
echo "<cert>" >> ~/"$client.ovpn"
sed -ne '/BEGIN CERTIFICATE/,$ p' /etc/openvpn/server/easy-rsa/pki/issued/"$client".crt >> ~/"$client.ovpn"
echo "</cert>" >> ~/"$client.ovpn"
echo "<key>" >> ~/"$client.ovpn"
cat /etc/openvpn/server/easy-rsa/pki/private/"$client".key >> ~/"$client.ovpn"
echo "</key>" >> ~/"$client.ovpn"
echo "<tls-crypt>" >> ~/"$client.ovpn"
sed -ne '/BEGIN OpenVPN Static key/,$ p' /etc/openvpn/server/tc.key >> ~/"$client.ovpn"
echo "</tls-crypt>" >> ~/"$client.ovpn"
echo "$client added. path:" ~/"$client.ovpn"
cat ~/"$client.ovpn"
exit